extends Control

# this could still be really dangerous, but seems to work just fine!

func _physics_process(delta: float) -> void:
	if Input.is_action_just_pressed("ui_home"):
		OS.window_fullscreen = !OS.window_fullscreen

func _on_Open_pressed() -> void:
	$"%FileDialog".popup()

func _on_Save_pressed() -> void:
	$"%FileDialog2".popup()

# Open
func _on_FileDialog_file_selected(path: String) -> void:
	var file = File.new()
	file.open(path, File.READ)
	$"%TextEdit".text = file.get_as_text()
	file.close()

# Save
func _on_FileDialog2_file_selected(path: String) -> void:
	var file = File.new()
	file.open(path, File.WRITE)
	assert(file.is_open())
	file.store_string($"%TextEdit".text)
	file.close()
