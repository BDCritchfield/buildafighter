extends Node

# Controls and keeps track of the current game state and relevant info.

# Storing a reference to the players in a globally accessible place allows any node -
# - to easily call their functions, regardless of heirarchy or position.

var player_1: TradActor
var player_2: Dummy

var stage: Node
var in_menu: bool
