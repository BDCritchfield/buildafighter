extends Node

var p1 : Character
var p1_state
var p1_pushback
var p2 : Character
var p2_state
var p2_pushback

func _ready() -> void:
	pass
	
func _physics_process(delta: float) -> void:
	if p1_state != null and p1 != null:
		if p1.state_machine.state.name == p1_state:
			if p1.animator.is_playing() and p1.current_hitpause == 0:
				if p1.facing_left:
					p1.move_and_slide(Vector2(p1_pushback, 0))
				else:
					p1.move_and_slide(Vector2(p1_pushback * -1, 0))
		else:
			p1_pushback = 0
				
func handle_pushback(target : Character, pushback, state_to_check : String):
	if target.name == "TestPlayer":
		p1 = target
		p1_state = state_to_check
		p1_pushback = pushback
	elif target.is_class("Dummy"):
		p2 = target
		p2_state = state_to_check
		p2_pushback = pushback
	else:
		print_debug("unrecognized entity tried to call handle_pushback()")
