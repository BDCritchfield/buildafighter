extends KinematicBody2D
class_name Character

var velocity := Vector2.ZERO
var xinput
var yinput

# Boilerplate class that all character types inherit from, to identify them as characters.
