extends Character
class_name TradCharacter, "res://icon.png"

export(Resource) var character_resource
var my_resource : resCharacter
export var unique_name : String
export var unique_number : int

# Attacks
onready var attacks = {
	"StandLightPunch" : 0,
	"AForward" : 0,
	"ADownForward" : 0,
	"ADown" : 0,
	"ADownBack" : 0,
	"ABack" : 0,
	"AUpBack" : 0,
	"AUp" : 0,
	"AUpForward" : 0,

	"AAirNeutral" : 0,
	"AAirForward" : 0,
	"AAirDownForward" : 0,
	"AAirDown" : 0,
	"AAirDownBack" : 0,
	"AAirBack" : 0,
	"AAirUpBack" : 0,
	"AAirUp" : 0,
	"AAirUpForward" : 0,

	"BNeutral" : 0,
	"BForward" : 0,
	"BDownForward" : 0,
	"BDown" : 0,
	"BDownBack" : 0,
	"BBack" : 0,
	"BUpBack" : 0,
	"BUp" : 0,
	"BUpForward" : 0,

	"BAirNeutral" : 0,
	"BAirForward" : 0,
	"BAirDownForward" : 0,
	"BAirDown" : 0,
	"BAirDownBack" : 0,
	"BAirBack" : 0,
	"BAirUpBack" : 0,
	"BAirUp" : 0,
	"BAirUpForward" : 0,

	"CNeutral" : 0,
	"CForward" : 0,
	"CDownForward" : 0,
	"CDown" : 0,
	"CDownBack" : 0,
	"CBack" : 0,
	"CUpBack" : 0,
	"CUp" : 0,
	"CUpForward" : 0,

	"CAirNeutral" : 0,
	"CAirForward" : 0,
	"CAirDownForward" : 0,
	"CAirDown" : 0,
	"CAirDownBack" : 0,
	"CAirBack" : 0,
	"CAirUpBack" : 0,
	"CAirUp" : 0,
	"CAirUpForward" : 0,

	"DNeutral" : 0,
	"DForward" : 0,
	"DDownForward" : 0,
	"DDown" : 0,
	"DDownBack" : 0,
	"DBack" : 0,
	"DUpBack" : 0,
	"DUp" : 0,
	"DUpForward" : 0,

	"DAirNeutral" : 0,
	"DAirForward" : 0,
	"DAirDownForward" : 0,
	"DAirDown" : 0,
	"DAirDownBack" : 0,
	"DAirBack" : 0,
	"DAirUpBack" : 0,
	"DAirUp" : 0,
	"DAirUpForward" : 0,

	"MQCF" : 0,
	"MDP" : 0,
	"MQCB" : 0,
	"MHCF" : 0,
	"MHCB" : 0,

	"MAirQCF" : 0,
	"MAirDP" : 0,
	"MAirQCB" : 0,
	"MAirHCF" : 0,
	"MAirHCB" : 0,
}

# Hitboxes
onready var states_list = [
	"Idle",
	"Block",
	"Crouch",
	"Move",
	"Dash",
	"Jumpstart",
	"Air",
	"Airdash",
	"Flinch",
	"AirFlinch",
	"Launch",
	"Knockdown",
	"SLPunch",
	"CLPunch",
	"JLPunch",
	"SMPunch",
	"CMPunch",
	"SHPunch",
	"CHPunch",
	"JHPunch",
	"SLKick",
	"CLKick",
	"JLKick",
	"SMKick",
	"CMKick",
	"JMKick",
	"SHKick",
	"CHKick",
	"JHKick",
	"LQCF",
	"MQCF",
	"HQCF",
	"LDP",
	"MDP",
	"HDP",
	"LQCB",
	"MQCB",
	"HQCB",
	"LHCF",
	"MHCF",
	"HHCF",
]

# Sprites
onready var sprites = {
	"Idle" : null,
	"StandA" : null,
	"StandB" : null,
}

# VFX
onready var vfx = {
	"Idle" : null,
	"hit01" : null,
}

# SFX
onready var sfx = {
	"Idle" : null,
}

# Variables

# Combo Data
var combo_count : int = 0
var in_hitstun := false
onready var current_hitpause := 0
onready var current_pushback := 0

# State Info
var state_duration : int
var state : float

# Framedata / Constants
var jumpstart_frames := 4
var jumpstart_frames_back := 3
var landing_frames := 4 # cancellable by most actions EXCEPT jump
var dash_frames := 14

