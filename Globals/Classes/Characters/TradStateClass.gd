extends Node
class_name TradState
# This class provides the individual nodes with a StateMachine variable
# as well as defines their functions.

var state_machine : StateMachine
var player : TradActor

var state_type : String
var state_subcat : String

# Variables
var can_block : bool


func handle_input(_event: InputEvent):
	pass
	
func update(_delta):
	pass
	
func physics_update(_delta):
	pass
	
func enter():
	pass
	
func exit():
	pass
	
func _frame_update(type):
	match type:
		0:# Enter
			pass
		1:# Exit
			player.state_duration = 0
		2:# Physics Update
			if player.current_hitpause <= 0: 
				player.state_duration += 1

func turnaround_check():
	if player.global_position.x < Game.player_2.global_position.x: # to the left of the enemy
		if player.facing_left:
			player.turn_around()
		else:
			pass
	else: # to the right of the enemy
		if player.facing_left:
			pass
		else:
			player.turn_around()

func check_block():
	if !player.facing_left:
		if InputBuffer.direction_this_frame == 4:
			state_machine.change("Block")
	else:
		if InputBuffer.direction_this_frame == 6:
			state_machine.change("Block")
