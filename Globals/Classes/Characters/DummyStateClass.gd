extends Node
class_name DummyState
# This class provides the dummy with its own state machine and variables.

var state_machine : DummyStateMachine
var dummy : Dummy


func handle_input(_event: InputEvent):
	pass
	
func update(_delta):
	pass
	
func physics_update(_delta):
	pass
	
func enter():
	pass
	
func exit():
	pass
	
func _frame_update(type):
	match type:
		0:# Enter
			pass
		1:# Exit
			dummy.state_duration = 0
		2:# Physics Update
			dummy.state_duration += 1

func turnaround_check():
	if dummy.global_position.x < Game.player_1.global_position.x: # to the left of the player
		if dummy.facing_left:
			dummy.turn_around()
		else:
			pass
	else: # to the right of the player
		if dummy.facing_left:
			pass
		else:
			dummy.turn_around()
