extends Node2D
class_name Hitbox

var creator : Character
var Owner : Character # must be capitalized, "owner" is a built-in variable.
export var pos_offset := Vector2.ZERO
export var flipped := false

export var duration := 0
export var frame := 0

# Stats
export var damage := 0
export var hitpause := 0
export var hitstun := 0
export var knockback : float = 0
export var knockback_angle := 0
export var pushback : float = 0

export var strength := 10
export var trample := false

# True/Falses
export var projectile := false
export var launcher := false
export var knockdown := false
export var ground_bounce := false
export var wall_bounce := false
export var hits_on_ground := false

# Combo Data
export var juggle_value := 0
export var juggle_max := 0
export var gravity_value := 0
export var hitstun_value := 0

# Functions

func _physics_process(delta: float) -> void:
	if frame >= duration:
		queue_free()
	
	frame += 1
