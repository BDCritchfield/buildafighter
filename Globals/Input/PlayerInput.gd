extends Node

var player_1
var player_2

var buffer = []
var max_buffer_size = 30
var previous_input

var diagonal: bool
var input = Vector2.ZERO
var direction

func _physics_process(_delta):
	var myarray = get_input()
	diagonal = myarray[0]
	input.x = myarray[1]
	input.y = myarray[2]
	direction = myarray[3]

func get_input():
	var check_diagonal = false
	var input_x = Input.get_action_strength("right") - Input.get_action_strength("left")
		
	var input_y = Input.get_action_strength("down") - Input.get_action_strength("up")
	
		
	var temp_input = Vector2( input_x , input_y ).normalized()
	if abs(temp_input.x) != 1 and abs(temp_input.x) != 0:
		check_diagonal = true
	if abs(temp_input.y) != 1 and abs(temp_input.y) != 0:
		check_diagonal = true
	var temp_direction = get_direction(check_diagonal, temp_input.x, temp_input.y)
	
	return [ check_diagonal, temp_input.x , temp_input.y , temp_direction ]

func get_direction(diagonal_, input_x, input_y):
	if !diagonal_:
		match sign(input_x):
			0.0:
				match sign(input_y):
					1.0:
						return 2
					-1.0:
						return 8
					0.0:
						return 0
			1.0:
				return 6
			-1.0:
				return 4
	else:
		match sign(input_x):
			0.0:
				return 0 # this shouldnt happen
			1.0:
				if sign(input_y) == 1:
					return 3
				else:
					return 9
			-1.0:
				if sign(input_y) == 1:
					return 1
				else:
					return 7
