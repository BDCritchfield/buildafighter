extends Node

onready var direction_this_frame: float
onready var previous_value: float
onready var buffer = []

func _input(_event):
	pass
#	if event.is_action_pressed("ui_cancel"):
#		print_debug(direction_this_frame)
#		print_debug(PlayerInput.diagonal)

func _physics_process(_delta):
	if PlayerInput.input != Vector2.ZERO:
		if PlayerInput.direction != previous_value:
			direction_this_frame = PlayerInput.direction
		else:
			direction_this_frame = PlayerInput.direction
	else:
		direction_this_frame = 0
		
	buffer.append(direction_this_frame)
	previous_value = direction_this_frame
