tool
extends EditorPlugin

const MainPanel = preload("res://addons/screenres/main.tscn")

var main_panel_instance

onready var resolution_list = []

func _enter_tree() -> void:
	print_debug("screen resolution changer plugin active")
	main_panel_instance = MainPanel.instance()
	
	var game_height = var2str(ProjectSettings.get_setting("display/window/size/height"))
	var game_width = var2str(ProjectSettings.get_setting("display/window/size/width"))
	var test_height = var2str(ProjectSettings.get_setting("display/window/size/test_height"))
	var test_width = var2str(ProjectSettings.get_setting("display/window/size/test_width"))
	
	var size_label = main_panel_instance.get_node("VBoxContainer/Label")
	var test_label = main_panel_instance.get_node("VBoxContainer/Label2")
	
	size_label.text = "Current Gamesize: " + game_width + "x" + game_height
	test_label.text = "Current Test Resolution: " + test_width + "x" + test_height
	
	get_editor_interface().get_editor_viewport().add_child(main_panel_instance)
	make_visible(false)

func _process(delta: float) -> void:
	var game_height = var2str(ProjectSettings.get_setting("display/window/size/height"))
	var game_width = var2str(ProjectSettings.get_setting("display/window/size/width"))
	var test_height = var2str(ProjectSettings.get_setting("display/window/size/test_height"))
	var test_width = var2str(ProjectSettings.get_setting("display/window/size/test_width"))
	
	var size_label = main_panel_instance.get_node("VBoxContainer/Label")
	var test_label = main_panel_instance.get_node("VBoxContainer/Label2")
	
	size_label.text = "Current Gamesize: " + game_width + "x" + game_height
	test_label.text = "Current Test Resolution: " + test_width + "x" + test_height

func _exit_tree() -> void:
	if main_panel_instance:
		main_panel_instance.queue_free()

func has_main_screen() -> bool:
	return true

func make_visible(visible: bool) -> void:
	if main_panel_instance:
		main_panel_instance.visible = visible

func get_plugin_name() -> String:
	return "Screen Resolution"

func get_plugin_icon() -> Texture:
	return get_editor_interface().get_base_control().get_icon("Node", "EditorIcons")
