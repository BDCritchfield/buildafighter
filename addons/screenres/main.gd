tool
extends Control

onready var default_size = Vector2(640, 360)
onready var default_resolution = Vector2(1920, 1080)

onready var temp_size = Vector2.ZERO
onready var temp_resolution = Vector2.ZERO

onready var game_size_label = $VBoxContainer/Label
onready var test_res_label = $VBoxContainer/Label2

onready var resolution_dict = {
	"640x360" : Vector2(640, 360),
	"854x480" : Vector2(854, 480),
	"960x540" : Vector2(960, 540),
	"1280x720" : Vector2(1280, 720),
	"1366x768" : Vector2(1366, 768),
	"1600x900" : Vector2(1600, 900),
	"1920x1080" : Vector2(1920, 1080),
	"2560x1440" : Vector2(2560, 1440),
	"3840x2160" : Vector2(3840, 2160),
}

func _ready() -> void:
	if Engine.editor_hint:
		print_debug("menu scene ready")
		var game_list = $VBoxContainer/HBoxContainer2/VBoxContainer
		var res_list = $VBoxContainer/HBoxContainer2/VBoxContainer2
		for i in game_list.get_children():
			if !i is ResolutionButton:
				pass
			else:
				i.connect("pressed", self, "game_size_changed", [i.text])
		for i in res_list.get_children():
			if !i is ResolutionButton:
				pass
			else:
				i.connect("pressed", self, "test_resolution_changed", [i.text])

func game_size_changed(new_size):
	print_debug("test size has been changed! the new value is: " + new_size)
	for i in resolution_dict.keys():
		if new_size != i:
			pass
		else:
			temp_size = resolution_dict[new_size]
			return

func test_resolution_changed(new_resolution : String):
	print_debug("test resolution has been changed! the new value is: " + new_resolution)
	for i in resolution_dict.keys():
		if new_resolution != i:
			pass
		else:
			temp_resolution = resolution_dict[new_resolution]
			return

func _on_Apply_pressed() -> void:
	if temp_size.x == ProjectSettings.get_setting("display/window/size/width") and temp_size.y == ProjectSettings.get_setting("display/window/size/height"):
		pass
	elif temp_size.x == 0 and temp_size.y == 0:
		pass
	else:
		ProjectSettings.set_setting("display/window/size/width", temp_size.x)
		ProjectSettings.set_setting("display/window/size/height", temp_size.y)
	
	if temp_resolution.x == ProjectSettings.get_setting("display/window/size/test_width") and temp_size.y == ProjectSettings.get_setting("display/window/size/test_height"):
		pass
	elif temp_resolution.x == 0 and temp_resolution.y == 0:
		pass
	else:
		ProjectSettings.set_setting("display/window/size/test_width", temp_resolution.x)
		ProjectSettings.set_setting("display/window/size/test_height", temp_resolution.y)
		
	temp_size = Vector2.ZERO
	temp_resolution = Vector2.ZERO

func _on_Reset_pressed() -> void:
	ProjectSettings.set_setting("display/window/size/width", default_size.x)
	ProjectSettings.set_setting("display/window/size/height", default_size.y)
	
	ProjectSettings.set_setting("display/window/size/test_width", default_resolution.x)
	ProjectSettings.set_setting("display/window/size/test_height", default_resolution.y)
