extends Node2D

onready var default_player = preload("res://Characters/Character0/TestPlayer.tscn")
onready var player_path : NodePath

# Filepaths need to be changed here.
# The save/load functionality here is good for saving changes to characters,
# but our other way should work too.

func _ready():
	var new_file = File.new()
	if !new_file.open("user://custom/characters/character0/character.tscn", 1) == OK:
		var player_instance = default_player.instance()
		player_instance.global_position = $PlayerSpawn.global_position
		add_child(player_instance)
	else:
		var load_player = load("user://custom/characters/character0/character.tscn")
		var player_instance = load_player.instance()
		player_instance.global_position = $PlayerSpawn.global_position
		add_child(player_instance)
#	var dir = Directory.new()
#	if !dir.open("user://savedata/save_01") == OK:
#		player_path = "res://Characters/Default/TestPlayer.tscn"
#		var player = default_player.instance()
#		add_child(player)
#	else:
#		load_game()
		

func _unhandled_input(event):
	if event.is_action_pressed("ui_home"):
		OS.window_fullscreen = !OS.window_fullscreen

func save():
	var dir = Directory.new()
	if !dir.open("user://savedata/save_01") == OK:
		dir.make_dir_recursive("user://savedata/save_01")
	else:
		pass
		
	var new_save = File.new()
	new_save.open("user://savedata/save_01/test.save", File.WRITE)
	var nodes_to_save = get_tree().get_nodes_in_group("Save")
	for node in nodes_to_save:
		if node.filename == null:
			print_debug("node is not an instanced scene, skipping.")
			continue
		if !node.has_method("save"):
			print_debug("node does not have a save() function, skipping.")
			continue
		var node_data = node.call("save")
		new_save.store_line(to_json(node_data))
	new_save.close()

func load_game():
	var save_game = File.new()
	if !save_game.file_exists("user://savedata/save_01/test.save"):
		return # Error! no save
	
	var save_nodes = get_tree().get_nodes_in_group("Save")
	if save_nodes == null:
		pass
	else:
		for i in save_nodes:
			i.queue_free()
		
	save_game.open("user://savedata/save_01/test.save", File.READ)
	while save_game.get_position() < save_game.get_len():
		var node_data = parse_json(save_game.get_line())
		
		var new_object = load(node_data["filename"]).instance()
		get_node(node_data["parent"]).add_child(new_object)
		new_object.position = Vector2(node_data["position.x"], node_data["position.y"])
		player_path = get_path_to(new_object)
		
		for i in node_data.keys():
			if i == "filename" or i == "parent" or i == "position.x" or i == "position.y":
				continue
			new_object.set(i, node_data[i])
			
	save_game.close()

func _on_SaveGame_pressed():
	pass
#	save()
	

func _on_ChangeStats_pressed():
	pass
#	get_node(player_path).speed = 200
#	get_node(player_path).health = 50


func _on_Load_pressed():
	pass
#	load_game()
