tool
extends Node

onready var states_list
onready var number
onready var array_number
onready var source_code

func _ready():
	
	number = 0
	array_number = 0
	
	states_list = [
	"Idle",
	"Block",
	"Crouch",
	"Move",
	"Dash",
	"Jumpstart",
	"Air",
	"Airdash",
	"Grab",
	"Grabbed",
	"Flinch",
	"AirFlinch",
	"Launch",
	"Knockdown",
	"SLPunch",
	"CLPunch",
	"JLPunch",
	"SMPunch",
	"CMPunch",
	"JMPunch",
	"SHPunch",
	"CHPunch",
	"JHPunch",
	"SLKick",
	"CLKick",
	"JLKick",
	"SMKick",
	"CMKick",
	"JMKick",
	"SHKick",
	"CHKick",
	"JHKick",
	"LQCF",
	"MQCF",
	"HQCF",
	"LDP",
	"MDP",
	"HDP",
	"LQCB",
	"MQCB",
	"HQCB",
	"LHCF",
	"MHCF",
	"HHCF",
	
]
	source_code = "extends PlayerState \n \nfunc enter(): \n	player.animator.play(name) \n \nfunc handle_input(event): \n	pass \n \nfunc physics_update(delta): \n	_frame_update(2) \n \nfunc exit(): \n	pass"

#  

#func _process(_delta):
#	if Input.is_action_just_pressed("ui_up"):
#		if Engine.editor_hint:
#			_ready()
#			var my_children = $StateMachine.get_children() 
#			var new_child_list = []
#			for i in my_children:
#				new_child_list.append(i.name)
#			for i in states_list: 
#				if new_child_list.has(i):
#					print_debug("already has " + i)
#				else:
#					print_debug("trying to add " + i)
#					var new_state = Node.new()
#					new_state.name = i
#					var new_script = GDScript.new()
#					new_script.source_code = source_code
#					ResourceSaver.save("res://Characters/Character0/StateMachine/States/" + i + ".gd" , new_script)
#					$StateMachine.add_child(new_state)
#					new_script = ResourceLoader.load("res://Characters/Character0/StateMachine/States/" + i + ".gd")
#					new_state.set_script(new_script)
#					new_state.set_owner(get_tree().get_edited_scene_root())
#					new_state.set_process(true)
#					if $StateMachine.has_node(i):
#						print_debug("successfully added " + i)
