extends DummyState
class_name DummyStateConnector

func _ready():
	yield(owner, "ready")
	dummy = owner as Dummy
	assert(dummy != null)
