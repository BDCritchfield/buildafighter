extends DummyStateConnector

# Idle dummy

func enter() -> void:
	dummy.animator.play(name)
	
func handle_input(event : InputEvent) -> void:
	pass
	
func physics_update(delta) -> void:
	turnaround_check()
	
	dummy.state_duration += 1
	
	if !dummy.touching_enemy:
		dummy.velocity.x = 0
	else:
		pass

	dummy.velocity = dummy.move_and_slide(dummy.velocity, Vector2.UP)
#	if player.is_on_floor() == false:
#		state_machine.change("Air")
	
func exit() -> void:
	pass
