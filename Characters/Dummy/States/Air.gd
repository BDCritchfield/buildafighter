extends DummyStateConnector

# Airborne

func enter() -> void:
	pass
	
func handle_input(event) -> void:
	pass
	
func physics_update(delta) -> void:
	player.velocity.y += player.gravity * delta # Gravity
	player.velocity.y = clamp(player.velocity.y, -player.fall_speed, player.fall_speed)
	
	player.velocity.x = clamp(player.velocity.x, -player.max_air_speed, player.max_air_speed)
	
	player.velocity = player.move_and_slide(player.velocity, Vector2.UP)
	if player.is_on_floor():
		state_machine.change("Idle")
	
func exit() -> void:
	pass
