extends DummyStateConnector

func enter():
	dummy.sprite.position = Vector2(-64, -104)
#	if dummy.facing_left:
#		dummy.velocity.x = dummy.current_knockback
#	else:
#		dummy.velocity.x = dummy.current_knockback * -1
	dummy.animator.play(name)
	
func handle_input(_event):
	pass
	
func physics_update(_delta):
	_frame_update(2)
	
	if dummy.state_duration >= dummy.current_hitstun:
		state_machine.change("Idle")
	else:
		dummy.move_and_slide(dummy.velocity, Vector2.UP)
	
func exit():
	dummy.current_hitstun = 0
	dummy.current_knockback = 0
