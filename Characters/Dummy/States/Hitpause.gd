extends DummyStateConnector

func enter():
	dummy.animator.play(name)
	
func handle_input(_event):
	pass
	
func physics_update(_delta):
	_frame_update(2)
	
	if dummy.state_duration >= dummy.current_hitpause:
		dummy.state_machine.change("Flinch")
	
func exit():
	dummy.current_hitpause = 0
