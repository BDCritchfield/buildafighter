extends Character
class_name Dummy

# Get child nodes as variables for convenience 
#onready var camera = $"%Camera2D"
onready var sprite_pos = $SpritePos
onready var sprite = $"%Sprite"
onready var ECB = $"%CollisionShape2D" # environmental collision box
onready var raycast = $"%RayCast2D"
onready var animator = $"%AnimationPlayer"
onready var stand_point = $"%StandPoint"
onready var motion_input = $"%MotionInput"
onready var state_machine = $"%StateMachine"
onready var audio_player = $AudioStreamPlayer

# Movement

# State Info
var state_duration : int
var state : float

# Framedata / Constants
var jumpstart_frames := 4
var jumpstart_frames_back := 3
var landing_frames := 4 # cancellable by most actions EXCEPT jump

# Variables
var touching_enemy := false
var facing_left := false
var invincible_physical := false
var invincible_projectile := false
var invincible_full := false
export var max_speed := 200
export var jump_force := 200
export var max_air_speed := 200
export var double_jump_force := 200
export var gravity := 1000
export var fall_speed := 200

# Combo Status
onready var combo_count := 0.0
onready var juggle_count := 0.0
onready var current_hitpause := 0.0
onready var in_hitpause := false
onready var current_hitstun := 0.0
onready var in_hitstun := false
onready var current_knockback := 0

onready var hit_fx = preload("res://Characters/Character0/Sprites/hit_fx_01.tscn")

func _ready() -> void:
	yield(get_parent(), "ready")
	if get_parent().is_in_group("Stage"):
#		camera.current = true
		Game.player_2 = self
		
func _physics_process(delta: float) -> void:
	#Game.player_1.function for autocomplete
	
	raycast.force_raycast_update()
	if raycast.is_colliding():
		touching_enemy = true
	else:
		touching_enemy = false
		
	pause_mode = PAUSE_MODE_STOP
#	xinput = 0
#	yinput = 0

func got_hit(hitpause, hitstun, knockback, juggle_add):
	audio_player.play()
	add_child(hit_fx.instance())
	
	juggle_count += juggle_add
	current_hitpause = hitpause
	current_hitstun = hitstun
	
	apply_knockback(knockback)
	state_machine.change("Hitpause")
	print_debug("training dummy hit")
	
func apply_knockback(knockback):
	if facing_left:
		velocity.x += knockback
	else:
		velocity.x += knockback * -1
	
func  turn_around():
	facing_left = !facing_left
	
	match facing_left:
		true:
			sprite_pos.scale.x = -1
		false:
			sprite_pos.scale.x = 1
