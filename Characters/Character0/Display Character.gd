extends Node2D

func _physics_process(delta: float) -> void:
	if Input.is_action_just_pressed("ui_home"):
		OS.window_fullscreen = !OS.window_fullscreen


func _on_Button_pressed() -> void:
	get_tree().change_scene("res://Menus/EditCharacter/Example/SelectAttack.tscn")
