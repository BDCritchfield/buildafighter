extends Node
class_name StateMachine
# The state handler 

# Make sure to set the Initial State in the editor before running !!!!!!
export var initial_state : NodePath
onready var state = get_node(initial_state)

onready var player

func _ready():
	yield(owner, "ready")
	player = owner
	
	# Assigns self to each state's state_machine property
	for child in get_children():
		if !"state_machine" in child:
			print_debug("Some children don't have state_machine variables!")
		else:
			child.state_machine = self
	state.enter()
	
func _unhandled_input(event):
	state.handle_input(event)
	
func _process(delta):
	state.update(delta)
	
func _physics_process(delta):
	state.physics_update(delta)
	
func change(new_state: String):
	if !has_node(new_state):
		print_debug("tried to switch to an invalid state")
		return
		
	state.exit()
	state._frame_update(1)
	state = get_node(new_state)
	player.state = state.name
	state.enter()
	state._frame_update(0)
	
	if "label" in player:
		player.label.text = new_state
#	emit_signal("transition", state.name)
	print_debug(new_state)


#func add_states(states_list : Array):
#	var my_children = get_children()
#	print(my_children[0].name)
#	for i in states_list:
#		if my_children.has(i):
#			print("already has " + i )
#		else:
#			print("trying to add " + i)
#			var new_state = Node.new()
#			new_state.name = i
#			var new_script = Script.new()
#			add_child(new_state)
#			new_state.set_owner(self)
#			new_state.set_script(new_script)
#			new_state.set_process(true)
