extends PlayerState

# Block

func enter() -> void:
	player.velocity.x = 0
	player.sprite.modulate = Color(0.317647, 0.345098, 0.941176)
	
func handle_input(_event : InputEvent) -> void:
	pass
	
func physics_update(_delta) -> void:
	_frame_update(2)
	
	if !player.facing_left:
		if InputBuffer.direction_this_frame != 4:
			state_machine.change("Idle")
	else:
		if InputBuffer.direction_this_frame != 6:
			state_machine.change("Idle")
	
	player.velocity = player.move_and_slide(player.velocity, Vector2.UP)
	
func exit() -> void:
	player.sprite.modulate = Color(1, 1, 1)
