extends PlayerState
 
func enter():
	player.velocity.y = 0
	player.velocity.x = sign(player.xinput) * player.dash_speed
	player.sprite.self_modulate = Color(0.913725, 0.933333, 0.12549)
 
func handle_input(event): 
	pass 
 
func physics_update(delta): 
	_frame_update(2) 
	
	if player.state_duration >= player.dash_frames:
		state_machine.change("Air")
	
	player.move_and_slide(player.velocity, Vector2.UP)
func exit(): 
	player.sprite.self_modulate = Color(1, 1, 1)
