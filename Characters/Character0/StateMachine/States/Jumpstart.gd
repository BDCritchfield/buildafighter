extends PlayerState
 
func enter(): 
	pass
 
func handle_input(event): 
	pass 
 
func physics_update(delta): 
	_frame_update(2) 
	
	if player.state_duration >= player.jumpstart_frames:
		player.velocity.y += -player.jump_force
		player.jump_type = sign(player.xinput)
		state_machine.change("Air")
	
	player.velocity = player.move_and_slide(player.velocity, Vector2( 0 , -1 ))
func exit(): 
	pass
