extends PlayerState

# Idle

func enter() -> void:
	player.velocity.x = 0
	player.animator.play("Idle")
	
	state_type = "GroundFree"
	state_subcat = ""
	can_block = true
	
func handle_input(event : InputEvent) -> void:
	if event.is_action_pressed("up"):
		state_machine.change("Jumpstart")
	if event.is_action_pressed("down"):
		state_machine.change("Crouch")
		
	if event.is_action_pressed("light_punch"):
		player.grab_timer.start()
		state_machine.change("SLPunch")
	if event.is_action_pressed("medium_punch"):
		if player.grab_timer.time_left > 0:
			state_machine.change("Grab")
		else:
			state_machine.change("SMPunch")
	if event.is_action_pressed("heavy_punch"):
		state_machine.change("SHPunch")
		
	if event.is_action_pressed("light_kick"):
		state_machine.change("SLKick")
	if event.is_action_pressed("medium_kick"):
		state_machine.change("SMKick")
	if event.is_action_pressed("heavy_kick"):
		state_machine.change("SHKick")
		
	if event.is_action_pressed("simple_special"):
		state_machine.change("LQCF")
func physics_update(delta) -> void:
	turnaround_check()
	
	if player.xinput == 0: # friction and movement
		player.velocity.x = 0
	else:
		state_machine.change("Move")
		
	player.velocity = player.move_and_slide(player.velocity, Vector2.UP)
	if player.is_on_floor() == false:
		state_machine.change("Air")
	
func exit() -> void:
	pass
