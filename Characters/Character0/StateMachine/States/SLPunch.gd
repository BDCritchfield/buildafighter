extends PlayerState 
 
func enter(): 
	player.animator.play(name)
	player.velocity.x = 0
 
func handle_input(event): 
	pass 
 
func physics_update(delta):
	if player.animator.is_playing():
		_frame_update(2) 
		player.move_and_slide(player.velocity, Vector2.UP)
# check for the current attack_state, startup, active, or endlag
func exit(): 
	pass
