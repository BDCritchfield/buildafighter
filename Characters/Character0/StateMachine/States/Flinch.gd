extends PlayerState 
 
func enter(): 
	pass 
 
func handle_input(event): 
	pass 
 
func physics_update(delta): 
	_frame_update(2) 
	
	if player.velocity.x != 0:
		player.velocity.x = lerp(player.velocity.x, 0, player.pushback_friction)
	
	player.move_and_slide(player.velocity, Vector2.UP)
 
func exit(): 
	player.current_pushback = 0
