extends PlayerState

# Airborne

func enter() -> void:
	pass
	
func handle_input(event : InputEvent) -> void:
	if event.is_action_pressed("dash"):
		if sign(PlayerInput.input.x) != 0:
			state_machine.change("Airdash")
	
func physics_update(delta) -> void:
	player.velocity.y += player.gravity * delta # Gravity
	player.velocity.y = clamp(player.velocity.y, -player.jump_force, player.fall_speed)
	
	player.velocity.x = player.max_air_speed * player.jump_type
	player.velocity.x = clamp(player.velocity.x, -player.max_air_speed, player.max_air_speed)
	
	player.velocity = player.move_and_slide(player.velocity, Vector2.UP)
	if player.is_on_floor():
		state_machine.change("Idle")
	
func exit() -> void:
	player.jump_type = 0
