extends PlayerState

# Move

func enter() -> void:
	pass
	
func handle_input(event : InputEvent) -> void:
	if event.is_action_pressed("up"):
		state_machine.change("Jumpstart")
		
	if event.is_action_pressed("light_punch"):
		player.grab_timer.start()
		state_machine.change("SLPunch")
	if event.is_action_pressed("medium_punch"):
		if player.grab_timer.time_left > 0:
			state_machine.change("Grab")
		else:
			state_machine.change("SMPunch")
	if event.is_action_pressed("heavy_punch"):
		state_machine.change("SHPunch")
		
	if event.is_action_pressed("dash"):
		if sign(PlayerInput.input.x) != 0:
			state_machine.change("Dash")
	
func physics_update(delta) -> void:
	turnaround_check()
#	check_block()
	
	if player.xinput == 0: # friction and movement
		state_machine.change("Idle")
	else:
		player.velocity.x += sign(player.xinput) * player.max_speed
		player.velocity.x = clamp(player.velocity.x, -player.max_speed, player.max_speed)
	
	player.velocity = player.move_and_slide(player.velocity, Vector2( 0 , -1 ))
	if !player.is_on_floor():
		state_machine.change("Air")
		
func exit() -> void:
	pass
