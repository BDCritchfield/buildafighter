extends PlayerState

# Crouch

func enter() -> void:
	pass
	
func handle_input(event : InputEvent) -> void:
	if event.is_action_released("down"):
		state_machine.change("Idle")
	
func physics_update(delta) -> void:
	_frame_update(2)
	turnaround_check()
	
	if player.xinput == 0: # friction and movement
		player.velocity.x = 0
	else:
		state_machine.change("Move")
	
	player.velocity = player.move_and_slide(player.velocity, Vector2.UP)
	
func exit() -> void:
	pass
