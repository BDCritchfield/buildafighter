extends TradState
class_name PlayerState


func _ready():
	yield(owner, "ready")
	player = owner as TradActor
	assert(player != null)
