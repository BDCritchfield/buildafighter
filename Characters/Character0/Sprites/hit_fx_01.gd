extends Node2D

onready var duration = 10
onready var current_life = 0

func _ready() -> void:
	position = Vector2(-20, -10)
	scale = Vector2(2.4, 2.4)

func _physics_process(delta: float) -> void:
	current_life += 1
	
	if current_life >= duration:
		queue_free()
