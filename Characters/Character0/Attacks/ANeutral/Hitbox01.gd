extends Hitbox

#onready var player : Character
#onready var pos_offset := Vector2( 56 , -32 )
#onready var flipped : bool
#
#onready var duration := 2
#onready var frame := 0
#
## Stats
#onready var damage = 5
#onready var hitpause := 6.0
#onready var hitstun := 7.0
#
#onready var strength
#onready var trample
#
## True/Falses
#onready var projectile := false
#onready var launcher := false
#onready var knockdown := false
#onready var ground_bounce := false
#onready var wall_bounce := false
#onready var hits_on_ground := false
#
## Combo Data
#onready var juggle_value := 0.0
#onready var juggle_max := 3.0
#onready var gravity_value
#onready var hitstun_value

func _physics_process(delta: float) -> void:
	if frame >= duration:
		queue_free()
	
	frame += 1

# Functions

func _ready() -> void:
	knockback = 240
	
	if !flipped:
		position = pos_offset
	else:
		position.x = -pos_offset.x
		position.y = pos_offset.y

func _on_Area2D_area_entered(area: Area2D) -> void:
	var object_hit = area.get_parent()
	var invalid = false
	if object_hit.has_method("got_hit"):
		if object_hit.invincible_physical or object_hit.invincible_full:
			invalid = true
		if object_hit.juggle_count >= juggle_max:
			invalid = true
		
		if invalid:
			creator.enter_hitpause(hitpause, pushback)
		else:
			object_hit.got_hit(hitpause, hitstun, knockback, juggle_value)
			creator.enter_hitpause(hitpause, pushback)
			queue_free()

