extends Hitbox

# Functions

func _ready() -> void:
	connect("area_entered", self, "_on_Area2D_area_entered")
	
	if !flipped:
		position = pos_offset
	else:
		position.x = -pos_offset.x
		position.y = pos_offset.y

func _on_Area2D_area_entered(area: Area2D) -> void:
	var object_hit = area.get_parent()
	var invalid = false
	if object_hit.has_method("got_hit"):
		if object_hit.invincible_physical or object_hit.invincible_full:
			invalid = true
		if object_hit.juggle_count >= juggle_max:
			invalid = true
		
		if invalid:
			creator.enter_hitpause(hitpause, pushback)
		else:
			object_hit.got_hit(hitpause, hitstun, knockback, juggle_value)
			creator.enter_hitpause(hitpause, pushback)
			print_debug("hadouken hit")
			queue_free()
