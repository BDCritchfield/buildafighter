extends Node
class_name StateMachineNEW

var state_list = {
	"Idle" : "preload script here",
	"Move" : "preload script here",
	# etcetera
}

var state_current = state_list["Idle"]

# function examples
func _ready() -> void:
	state_current.ready()
	
# with this setup, we would only need to make scripts for the states, instead of entire nodes.
