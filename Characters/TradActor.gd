extends TradCharacter
class_name TradActor

# for saving
var current_position_x : float
var current_position_y : float

# Get child nodes as variables for convenience 
#onready var camera = $"%Camera2D"
onready var sprite := $"%Sprite"
onready var sprite_pos = $SpritePos
onready var CCB = $"%Pushbox"
onready var raycast = $"%RayCast2D"
onready var ECB = $"%ECB" # environmental collision box
onready var animator = $"%AnimationPlayer"
onready var stand_point = $"%StandPoint"
onready var motion_input = $"%MotionInput"
onready var state_machine = $"%StateMachine"
onready var label = $Label
# Timers
onready var grab_timer = $Timers/GrabTimer
onready var hitpause_timer = $Timers/HitpauseTimer

# Variables
var facing_left: = false
var jump_type: = 0
var shuffle_force: = 200

export var _c_Stats = 0
export(int) var base_health = 1000
export(int, 0, 1000) var health = 1000

export var _c_Ground_Movement = 0
export(float, 0, 1000, 5) var base_speed = 150.0
export(int, 0, 1000, 5) var max_speed = 200
export(float, 0, 1, 0.02) var friction
export(float, 0, 1, 0.05) var pushback_friction = 0.5
export(float, 0, 1000, 5) var dash_speed  = 400

export var _c_Air_Movement = 0
export(float) var jump_force = 500
export(float, 0, 1000) var max_air_speed = 500
export(float) var double_jump_force = 500
export(float) var gravity = 1600
export(float) var fall_speed = 400

export var _c_Variables = 0
export(int) var max_jumps = 1
export(int) var jump_count = 0

export var _c_State_Variables = 0
export var test = 0

func _ready() -> void:
	my_resource = character_resource
	set_inherited_variables()
	
	yield(get_parent(), "ready")
	if get_parent().is_in_group("Stage"):
#		camera.current = true
		Game.player_1 = self
	else:
		set_physics_process(false)

func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("save"):
		pass
		# add custom input. ex: super_attack, "U Key". actually, just add every possible controller key yourself.
		# maybe add all the possible button combinations too...?
		#
		#
	if event is InputEventMouseButton and event.button_index == 1:
			global_position = get_global_mouse_position()
			current_position_x = global_position.x
			current_position_y = global_position.y
			
	if event.is_action_pressed("editor_debug"):
		pass
#	if event.is_action_pressed("save"):
#		AccessProfile.add_character_to_profile("newguy")
#	if event.is_action_pressed("ui_home"):
#		AccessProfile.save_profile()
	
func _physics_process(delta: float) -> void:
	# x and y input
	xinput = sign(PlayerInput.input.x)
	yinput = sign(PlayerInput.input.y)
	
	if current_hitpause > 0:
		current_hitpause -= 1
	
	raycast.force_raycast_update()
	if raycast.is_colliding():
		max_speed = base_speed * .5
		var enemy = raycast.get_collider().get_parent()
		
		if enemy.global_position.x > global_position.x:
			enemy.velocity.x = base_speed * .5
		else:
			enemy.velocity.x = -base_speed * .5
	else:
		pass
		max_speed = base_speed
	
# Custom Functions

func spawn_hitbox( attack_name : String, hitbox_id : int ):
	var hitbox = attacks[attack_name][hitbox_id].instance()
	hitbox.flipped = facing_left
	hitbox.creator = self
	add_child(hitbox)
	
func turn_around():
	facing_left = !facing_left
	
	match facing_left:
		true:
			sprite_pos.scale.x = -1
		false:
			sprite_pos.scale.x = 1
	
func save() -> Dictionary:
	var variable_dict = {
		"filename" : filename,
		"parent" : get_parent().get_path(),
		"speed" : max_speed,
#		"health" : health,
		"position.x" : current_position_x,
		"position.y" : current_position_y
	}
	return variable_dict

func enter_hitpause(hitpause, pushback): # used when an attack you created successfully hits something.
	animator.stop(false)
	current_hitpause = hitpause
	current_pushback = pushback
	hitpause_timer.start(hitpause / 60.0)
	
func got_hit():
	# after doing some checks, call got_hit() in whatever the current state is.
	pass
	
func apply_pushback():
	if facing_left:
		velocity.x += current_pushback
	else:
		velocity.x += current_pushback * -1
	
func _on_Pushbox_area_entered(area: Area2D) -> void:
	pass

func _on_HitpauseTimer_timeout() -> void:
	animator.play()
	apply_pushback()
	current_hitpause = 0

func unpack_character_resource():
	pass

func set_inherited_variables():
	states_list = my_resource.move_list
	
	attacks = my_resource.hitbox_dict

	sprites = my_resource.sprite_list
	
	vfx = my_resource.vfx_dict
	
	sfx = my_resource.sfx_dict
