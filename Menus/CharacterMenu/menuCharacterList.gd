extends Node2D

# Get paths to child nodes for ease of access
# b = button, o = options menu, t = timer, e = text entry box, l = label

# this works for autocomplete :D
export var c_resource : Resource
var chara_res : resCharacter
var user_dir = "user://custom/profile/character_resources/"

# Character Options Menu
onready var character_menu = $CharacterMenu
onready var o_CharacterList = $CharacterMenu/CharacterList
onready var b_EditSelected = $CharacterMenu/EditSelected
onready var b_DeleteSelected = $CharacterMenu/DeleteSelected
onready var b_AddNewCharacter = $CharacterMenu/AddNew
# New Character Popup
onready var new_character_popup = $NewCharacterPopup
onready var e_NameEntry = $NewCharacterPopup/ColorRect3/NameEntry
onready var b_ConfirmNewCharacter = $NewCharacterPopup/ColorRect3/Create
onready var b_CancelNewCharacter = $NewCharacterPopup/ColorRect3/Cancel
onready var l_NewCharacterHint = $NewCharacterPopup/ColorRect3/Label2
# DELETE CHARACTER Popup
onready var DELETE_POPUP = $DELETEPOPUP
onready var l_DeleteWarning = $DELETEPOPUP/ColorRect/Label
onready var b_CancelDelete = $DELETEPOPUP/DELETECancel
onready var b_ConfirmDelete = $DELETEPOPUP/DELETE
# ERROR : Default Character Deletion
onready var l_DefaultCharacterDeletionError = $ColorRect/TriedToDeleteDefaultCharacter
onready var t_DefaultDeletionError = $"ColorRect/TTDDC Timer"

func _ready() -> void:
	AccessProfile.children_to_initialize.append(self)
	c_resource= ResourceLoader.load(user_dir + "Default" + ".tres")
	chara_res = c_resource as resCharacter

func initialize():
	if o_CharacterList.get_item_count() != 0:
		o_CharacterList.clear()
	var profile_list = AccessProfile.request_character_list()
	for i in profile_list:
		o_CharacterList.add_item(i.item_name, i.item_index)

# the below works perfectly
func _on_AddNew_pressed() -> void:
	new_character_popup.visible = true

func _on_Create_pressed() -> void:
	
	var new_clist_item = cListItem.new()
	new_clist_item.item_name = e_NameEntry.text
	new_clist_item.item_index = AccessProfile.request_number_of_characters()
	new_clist_item.scene_path = "res://Menus/Player Content/List Items/" + new_clist_item.item_name + ".tres"
	new_clist_item.is_builtin = false
	
	AccessProfile.test_add(new_clist_item, self)
	
	new_character_popup.visible = false
	e_NameEntry.text = ""

func _on_Cancel_pressed() -> void:
	new_character_popup.visible = false
	e_NameEntry.text = ""

func character_list_updated():
	initialize()

func _on_DeleteSelected_pressed() -> void:
	DELETE_POPUP.visible = true

func _on_DELETECancel_pressed() -> void:
	DELETE_POPUP.visible = false

func _on_DELETE_pressed() -> void:
	var id = o_CharacterList.get_selected_id()
	if o_CharacterList.get_item_text(id) == "Default":
		$ColorRect.visible = true
		t_DefaultDeletionError.start()
	else:
		var character_to_delete = AccessProfile.request_character_list()[id]
		AccessProfile.test_delete(character_to_delete, self)
		DELETE_POPUP.visible = false

func _on_TTDDC_Timer_timeout() -> void:
	$ColorRect.visible = false


func _on_Button_pressed() -> void:
	var res_to_save = resCharacter.new()
	res_to_save = chara_res
	
	# type the desired name of the file below without any spaces, prefixes, or file extensions
	var desired_file_name = "Default" 
	
	if ResourceSaver.save(user_dir + desired_file_name + ".tres", res_to_save) == 0:
		chara_res.user_path = user_dir + desired_file_name + ".tres"
		ResourceSaver.save(user_dir + desired_file_name + ".tres", res_to_save)
		print_debug("character resource saved successfully!")
	else:
		print_debug("uh oh")
