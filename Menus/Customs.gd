extends Node2D

onready var default_character = load("res://Characters/Default/TestPlayer.tscn")
onready var display = $"%CharacterDisplay"
onready var background = $"%MainMenu"
onready var character_dictionary = {
	"Character0" : 0
}


func Anti_Lian_Protocol() -> void:
	pass
	# if the players username is Lian, print I TOLD YOU NOT TO PLAY THIS GAME LIAN


func _ready() -> void:
	if !background.visible:
		background.visible = true
	
	pause_mode = 2
	for i in get_children():
		pause_mode = 2


# to add to dictionaries, just access the new value as if it were already in there.
func _on_New_pressed() -> void:
	# make new character instance
	var new_scene = default_character.instance()
	get_tree().paused = true
	new_scene.pause_mode = 1
	new_scene.visible = false
	
	# add it to the scene + edit it
	add_child(new_scene)
	var new_path = get_node(new_scene.name)
	new_path.max_speed = 2000
	
	# save it as its own scene
	var packed_scene = PackedScene.new()
	packed_scene.pack(new_path)
	print_debug(ResourceSaver.save("user://characters/character0/character.tscn", packed_scene))

func _on_Load_pressed() -> void:
	var packed_scene = load("user://characters/character0/character.tscn")
	var new_scene = packed_scene.instance()
	add_child(new_scene)
	var new_node = get_node(new_scene.name)
	print_debug(new_node.max_speed)
