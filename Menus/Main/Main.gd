extends Node2D

# CREATING, EDITING, AND SAVING NEW CHARACTERS IS WORKING!! WOOHOO!!
# character jostle physics are gonna need some work, but theyre working well for now. well enough
# could add a variable to states that you can jostle other characters in

# we could just make characters a custom resource...

# - TO-DO -
# Finish basic hit/combo system
# 	Create a system for pushback after being hit

# State Types:
# GroundFree, GroundCommit, AirFree, AirCommit, Hitstun 
# (hitstun means hitstun, knockdown, ground bounce, etc)

# Subcategories of all ground and air states:
# block, normal, special, enhanced, super
# Each attack must have three possible statuses, startup, active, and endlag, for an attack to check

# Counterhit Categories:
# the attack itself will check for the state of the enemy, and determine counterhits based on that

# could create sprite "groups" that are all loaded at one time. this would be for high res sprites.
# all the individual PNGs for a given move would be loaded at once. would this even do anything? 
# we could allow the user to Make sprite sheets. or just tell them how to do it themselves.

# - Blocking - working well enough - we can have each state list whether or not you are able to block while in it,
# and then block if an attack is approaching. or it checks if the person is blocking
# when it collides w them. i dunno.
# - Dash / Run - working

# Include proximity guard as a mechanical option. 



func _on_Exit_pressed():
	get_tree().quit()


func _on_Play_pressed():
	get_tree().change_scene("res://Stages/Default/DefaultStage.tscn")


func _on_Custom_pressed() -> void:
	pass # Replace with function body.
