extends Node2D

onready var o_MoveList = $ColorRect3/Moves

export(Resource) var ch_resource 
onready var character : resCharacter
onready var move_list = []

func _ready() -> void:
	character = ch_resource
	move_list = character.move_list
	
	var list_id = 0
	for i in move_list:
		o_MoveList.add_item(i, list_id)
		list_id += 1


func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_home"):
		OS.window_fullscreen = !OS.window_fullscreen

func _on_Button2_pressed() -> void:
	get_tree().change_scene("res://Menus/EditCharacter/Example/exampleCharacterEditScreen.tscn")
