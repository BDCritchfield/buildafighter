extends Control

# What we have to do 
# Add the attack Animation to the AnimationPlayer node, so the user can see it in action
# Look at each texture keyframe in the animations and assign them to the frame viewer sequentially
# Maybe create a stipped-down player scene without controls and stuff and put him in there

func _physics_process(delta: float) -> void:
	if Input.is_action_just_pressed("ui_home"):
		OS.window_fullscreen = ! OS.window_fullscreen

func _on_Return_to_previous_pressed() -> void:
	get_tree().change_scene("res://Menus/EditCharacter/Example/SelectAttack.tscn")
