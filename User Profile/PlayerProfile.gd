extends Node
#class_name PlayerProfile

signal profile_setup_complete

export var character_count := 1

func _ready() -> void:
	AccessProfile.profile_ready()
	AccessProfile.profile = self

export(Array, Resource) var character_list = [
	preload("res://Menus/CharacterMenu/List Items/Default_cListItem.tres")
]

export(Array, Resource) var character_resources = [
	preload("res://User Profile/Character Resources/resDefault.tres")
]
