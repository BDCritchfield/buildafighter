tool
extends Node

# Tool script to edit my character resources more easily

export var current_resource : Resource
onready var resource_data : resCharacter

#onready var moves = [
#	"Idle",
#	"Block",
#	"Crouch",
#	"Move",
#	"Dash",
#	"Jumpstart",
#	"Air",
#	"Airdash",
#	"Flinch",
#	"AirFlinch",
#	"Launch",
#	"Knockdown",
#	"SLPunch",
#	"CLPunch",
#	"JLPunch",
#	"SMPunch",
#	"CMPunch",
#	"SHPunch",
#	"CHPunch",
#	"JHPunch",
#	"SLKick",
#	"CLKick",
#	"JLKick",
#	"SMKick",
#	"CMKick",
#	"JMKick",
#	"SHKick",
#	"CHKick",
#	"JHKick",
#	"LQCF",
#	"MQCF",
#	"HQCF",
#	"LDP",
#	"MDP",
#	"HDP",
#	"LQCB",
#	"MQCB",
#	"HQCB",
#	"LHCF",
#	"MHCF",
#	"HHCF",
#]
#onready var sprites = {
#		"Idle" : preload("res://Characters/Character0/Attacks/ANeutral/stand_A.png"),
#		"StandA" : preload("res://Characters/Character0/Attacks/ANeutral/stand_A.png"),
#		"StandB" : preload("res://Characters/Character0/Attacks/BNeutral/stand_B.png"),
#}
#onready var vfx = {
#		"Idle" : null,
#		"hit01" : preload("res://Characters/Character0/Sprites/hit_vfx_01.png")
#}
#onready var sfx = {
#		"Idle" : null,
#}
#onready var hitboxes = {
#		"StandLightPunch" : [load("res://Characters/Character0/Attacks/ANeutral/Hitbox01.tscn"), "SecondHitbox"],
#		"AForward" : 0,
#		"ADownForward" : 0,
#		"ADown" : 0,
#		"ADownBack" : 0,
#		"ABack" : 0,
#		"AUpBack" : 0,
#		"AUp" : 0,
#		"AUpForward" : 0,
#
#		"AAirNeutral" : 0,
#		"AAirForward" : 0,
#		"AAirDownForward" : 0,
#		"AAirDown" : 0,
#		"AAirDownBack" : 0,
#		"AAirBack" : 0,
#		"AAirUpBack" : 0,
#		"AAirUp" : 0,
#		"AAirUpForward" : 0,
#
#		"BNeutral" : [load("res://Characters/Character0/Attacks/BNeutral/Hitbox01.tscn"), "secondhitbox"],
#		"BForward" : 0,
#		"BDownForward" : 0,
#		"BDown" : 0,
#		"BDownBack" : 0,
#		"BBack" : 0,
#		"BUpBack" : 0,
#		"BUp" : 0,
#		"BUpForward" : 0,
#
#		"BAirNeutral" : 0,
#		"BAirForward" : 0,
#		"BAirDownForward" : 0,
#		"BAirDown" : 0,
#		"BAirDownBack" : 0,
#		"BAirBack" : 0,
#		"BAirUpBack" : 0,
#		"BAirUp" : 0,
#		"BAirUpForward" : 0,
#
#		"CNeutral" : 0,
#		"CForward" : 0,
#		"CDownForward" : 0,
#		"CDown" : 0,
#		"CDownBack" : 0,
#		"CBack" : 0,
#		"CUpBack" : 0,
#		"CUp" : 0,
#		"CUpForward" : 0,
#
#		"CAirNeutral" : 0,
#		"CAirForward" : 0,
#		"CAirDownForward" : 0,
#		"CAirDown" : 0,
#		"CAirDownBack" : 0,
#		"CAirBack" : 0,
#		"CAirUpBack" : 0,
#		"CAirUp" : 0,
#		"CAirUpForward" : 0,
#
#		"DNeutral" : 0,
#		"DForward" : 0,
#		"DDownForward" : 0,
#		"DDown" : 0,
#		"DDownBack" : 0,
#		"DBack" : 0,
#		"DUpBack" : 0,
#		"DUp" : 0,
#		"DUpForward" : 0,
#
#		"DAirNeutral" : 0,
#		"DAirForward" : 0,
#		"DAirDownForward" : 0,
#		"DAirDown" : 0,
#		"DAirDownBack" : 0,
#		"DAirBack" : 0,
#		"DAirUpBack" : 0,
#		"DAirUp" : 0,
#		"DAirUpForward" : 0,
#
#		"MQCF" : [load("res://Characters/Character0/Attacks/QCF/Hitbox01.tscn"), "secondhitbox"], 
#		"MDP" : 0,
#		"MQCB" : 0,
#		"MHCF" : 0,
#		"MHCB" : 0,
#
#		"MAirQCF" : 0,
#		"MAirDP" : 0,
#		"MAirQCB" : 0,
#		"MAirHCF" : 0,
#		"MAirHCB" : 0,
#}

#func _ready() -> void:
#	if Engine.editor_hint:
#		print_debug("resource editor ready")
#		resource_data = current_resource
#
#
#func _physics_process(delta: float) -> void:
#	if Engine.editor_hint:
#		if Input.is_action_just_pressed("ui_cancel"): # escape button
#			_ready()
#			resource_data.move_list 
#			resource_data.sprite_list = sprites
#			resource_data.vfx_dict = vfx
#			resource_data.sfx_dict = sfx
#			resource_data.hitbox_dict = hitboxes
