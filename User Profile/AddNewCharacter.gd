extends Node
# Script that handles the creation of new custom player characters.

# create new kinematic body 
# add all the required nodes
#
#
#
# or
# create new instance of desired type of character script
# set script class_name to custom string that the player inputs?

func add_new_custom_character(_unique_name : String):
	pass
	var new_scene = KinematicBody2D.new()
#	var new_script = Script.new()
	ResourceSaver.save("res://Custom Characters/", new_scene)
	new_scene.set_script()
	# new character scene
	# make sure a function exists in all characters that saves their scene to disk
	# maybe add the nodepath to an array or dict
