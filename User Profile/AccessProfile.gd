extends Node

# Handles loading, editing, saving, etc for the actual Profile Node.
# This script is an AutoLoad and initiates the PlayerProfile
# DONT SAVE ANYTHING HERE

signal setup_success
onready var profile : Object

onready var children_to_initialize = []

onready var profile_scene_dir_PROJECT = "res://User Profile/PlayerProfile.tscn"
onready var profile_scene_dir_USER = "user://custom/profile/PlayerProfile.tscn"

onready var default_scene_dict = {
	"DefaultCharacter" : "user://custom/characters/character0",
	"DefaultStage" : "user://custom/stages/stage0",
}

func _ready() -> void:
	var startup_success = first_time_setup() # Add anything required for FIRST TIME setup in this function.
	
	if startup_success == true:
		emit_signal("setup_success")
		print("User data setup successfully!")
	else:
		print("CRITICAL GAME SETUP FAILURE. CHECK 'AccessProfile' SCRIPT")
	
	load_profile() # Handles "user profile" node

func profile_ready():
	for i in children_to_initialize:
		i.initialize()
		
func request_number_of_characters():
	var profile = get_node("/root/Profile")
	return profile.character_list.size()
	
func request_character_list():
	var profile = get_node("/root/Profile")
	var chara_list = profile.character_list
	return chara_list

func test_add(new_item : cListItem, caller):
	profile.character_list.append(new_item)
	print_debug(profile.character_list)
	save_profile()
	caller.character_list_updated()
	
func test_delete(selected_character : cListItem, caller):
	profile.character_list.remove(selected_character.item_index)
	print_debug(profile.character_list)
	save_profile()
	caller.initialize()
#	var dir = Directory.new()
#	if !dir.remove(selected_character.scene_path) == 0:
#		print_debug(dir.remove(selected_character.scene_path))
#		print_debug("Critical error trying to delete character!")
#	else:
#		print_debug("Successfully removed character " + "'" + selected_character.item_name + "'")

func add_character_to_profile(name : String):
	# Add name and path to profile dictionary
	
	var profile = get_node(".Profile")
	var new_chara_name = "character" + var2str(profile.character_list.size())
	profile.character_list[new_chara_name] = {
		"unique_name" : name ,
		 "path" : "user://custom/characters/" + new_chara_name}
	var dir = Directory.new()
	dir.make_dir_recursive("user://custom/characters/" + new_chara_name)
	
	# for debugging
	var dict_values = profile.character_list.values()
	var dict_size = profile.character_list.size()
	print_debug(dict_values[dict_size - 1])

func save_profile() -> void:
	var profile_temp = profile

	var packed_scene = PackedScene.new()
	packed_scene.pack(profile_temp)
	print_debug(ResourceSaver.save(profile_scene_dir_USER, packed_scene))
	
	var count = 0
	for i in profile.character_list:
		if i.item_index != count:
			i.item_index = count
		count += 1

func load_profile() -> void:
	# If there isn't a custom player profile in user://, load the default profile.
	var file = File.new()
	if !file.open("user://custom/profile/PlayerProfile.tscn", 1 ) == OK: # 1 means read only
		var packed_scene = load(profile_scene_dir_PROJECT)
		var new_scene = packed_scene.instance()
		get_parent().call_deferred("add_child", new_scene)
		print_debug("loading default")
	else:
		var packed_scene = load(profile_scene_dir_USER)
		var new_scene = packed_scene.instance()
		get_parent().call_deferred("add_child", new_scene)
		print_debug("loading custom")

func first_time_setup() -> bool:
	# check if the Default scene directories already exist, create if not. 
	# this should only ever happen Once.
	for i in default_scene_dict.values():
		var directory = Directory.new()
		if !directory.open(i) == OK:
			directory.make_dir_recursive(i)
			print_debug("created directory '" + i + "'")
		else:
			pass
			
	var file = File.new()
	if !file.open("user://custom/characters/character0/character0.tscn", 1) == OK:
		var player_scene = load("res://Characters/Character0/TestPlayer.tscn")
		var player_instance = player_scene.instance()
		var packed_scene = PackedScene.new()
		packed_scene.pack(player_instance)
		print(ResourceSaver.save("user://custom/characters/character0/character0.tscn", packed_scene))
		
	return true
