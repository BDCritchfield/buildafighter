extends Resource
class_name resCharacter

export var unique_name : String
export var list_index : int
export var custom : bool
export var character_scene : String
export var default_path : String
export var user_path : String

export(Array, String) var move_list 
export(Array, Texture) var sprite_list
export(Dictionary) var vfx_dict
export(Dictionary) var sfx_dict
export(Dictionary) var hitbox_dict

# Game Data
export var true_pos : Vector2





#func _get_property_list() -> Array:
#	pass

#func _get(property: String):
#	pass

#func _set(property: String, value) -> bool:
#	pass

